const express = require('express');
const { format } = require('date-fns')
const db = require('../db')
const router = express.Router();
const { Client } = require('@elastic/elasticsearch');
const fetch = require('node-fetch')
const geoJsonRewind = require('@mapbox/geojson-rewind')

const esClient = new Client({ node: 'http://localhost:9200' })
const esQueryBuilder = require('elastic-builder');
const { decodePolyLinetoCoordinates } = require('./decode-polyline');
const { randomUUID } = require('crypto')
const { toGeoJSON } = require('geojson-tools')
const _ = require('lodash')
const reducePoints = require('simplify-geometry');
const simplifyGeoJSON = require('simplify-geojson')
const { reduce } = require('lodash');

const turf = require('turf-multipolygon')


function recursiveFix(coords) {
  if (coords instanceof Array) {
    _.each(coords, function (coord, i) {
      if (coords[i] instanceof Array && coords[i].length > 0 && coords[i][0] instanceof Array) {
        // In a container
        coords[i] = recursiveFix(coord);
      } else if (i < coords.length - 1) {
        // In a point
        while (coords[i][0] == coords[i + 1][0] && coords[i][1] == coords[i + 1][1]) {
          coords.splice(i + 1, 1)
        }
      }
    });

    return coords;
  }
}



/* GET users listing. */
router.get('/get-sites', async function (req, res, next) {

  const sites = await db.query('select * from site order by siteid desc')

  const rows = sites.rows;

  const result = rows.map(row => {
    const { polygon } = row

    const poly = polygon.slice(1, -1).split("),").map((s) => s.slice(1).replace(")", "").split(","))

    return {
      ...row, polygon: poly
    }
  })

  res.json(result);
});


router.get('/get-site-data/:siteId', async (req, res, next) => {
  const { siteId } = req.params

  const sites = await db.query(`select * from site WHERE siteid='${siteId}'`)

  const row = sites.rows.length ? sites.rows[0] : { polygon: [] };

  const poly = row.polygon?.slice(1, -1)?.split("),")?.map((s) => s.slice(1)?.replace(")", "")?.split(","))

  res.json({ ...row, polygon: poly });
})

router.post('/store-site-polygon', async function (req, res, next) {
  const { polygon, siteid } = req.body

  const poly = `(${(polygon || []).map((ele) => `(${ele.join(",")})`).join(",")})`


  const result = await db.query(`UPDATE site SET polygon='${poly}'::polygon WHERE siteid='${siteid}'`)


  const indexedResponse = await fetch('http://localhost:3001/api/index-data');


  res.json(result)
});


router.get('/check-is-inside', async (req, res, next) => {

  const { lat, lng } = req.query


  const queryES = esQueryBuilder.requestBodySearch()
    .query(esQueryBuilder.boolQuery().filter(esQueryBuilder.geoShapeQuery("polygon").relation("INTERSECTS").shape(esQueryBuilder.geoShape("point", [Number(lng), Number(lat)]))))


  const responsES = await esClient.search({
    index: 'sites_data',
    body: queryES.toJSON()
  })



  res.json(responsES.body.hits.hits.map((hit) => ({ siteid: hit._source.siteid, name: hit._source.name })))
})


router.get('/index-data', async (req, res, next) => {
  const sites = await db.query(`select *,st_asgeojson(st_makevalid("polygon"::geometry,'keepcollapsed=false')) as valid_polygon from site`)

  const rows = sites.rows;

  const result = rows.map(row => {
    const { polygon, valid_polygon } = row
    const poly = JSON.parse(valid_polygon)?.geometries.find((x) => x.type === 'MultiPolygon' || x.type === 'Polygon')

    // console.log(JSON.parse(valid_polygon))
    //console.log(JSON.stringify(geoJsonRewind(poly)))

    const pFeat = turf(poly.coordinates)
    const testPolugon = simplifyGeoJSON(pFeat, 0.0001)


    console.log(testPolugon)
    console.log(`Total length : : `, JSON.stringify(testPolugon.geometry).length)

    const poly2 = testPolugon.geometry

    poly2.coordinates.map(x => x.map((y) => y.map(z => z.reverse())))

    const someThingisHere = poly2.coordinates.map((x) => x.map((y) => {

      const newArray = [[y[0][0], y[0][1]]]

      for (const val of y) {
        if (val[0] === newArray[newArray.length - 1][0] && val[1] === newArray[newArray.length - 1][1]) {
        } else {
          newArray.push([val[0], val[1]])
        }
      }

      return newArray
    }))
    // console.log(JSON.stringify(someThingisHere))

    const totalCoords = someThingisHere.map(x => x.filter((y) => y.length > 3)).filter(x => x.length > 0)


    const newPPPP = { ...poly, coordinates: totalCoords }


    //Array.from(new Set(polygon.slice(1, -1).split("),").map((s) => s.slice(1).replace(")", "")))).map(x => x.split(","))

    // const magic = [poly.coordinates.map((x => x.map(y => Number(y)).reverse()))]

    //recursiveFix(magic)

    //magic[0].pop()

    //const newPolygon = toGeoJSON(magic, 'Polygon')

    //console.log(JSON.stringify(newPolygon))
    // const first = newPolygon[0]
    // newPolygon.push(first)
    // console.log(JSON.stringify(newPolygon))
    return {
      siteid: row.siteid, name: row.name, polygon: newPPPP
    }
  })

  let response
  try {
    const body = result.flatMap((document) => [
      { index: { _index: 'sites_data', _id: document.siteid } },
      document,
    ]);

    response = await esClient.bulk({ refresh: true, body });

  } catch (err) {
    console.log(err)
  }

  res.json(response)
})


router.get('/create-site-polygon', async (req, res, next) => {
  const { lat, lng, distance, name } = req.query

  const [time, date, apiKey] =
    [
      format(new Date(), 'HH:mm:ss'),
      format(new Date(), 'yyyy / MM / dd'),
      'P5_KumUf8nilu3jq_16RYXCKdlnfjzFs_F6fW8aB2CQ']


  const polygonCoordinates = await fetch(`https://api.oalley.fr/api/AppKeys/${apiKey}/isodistance?mode=car&distance=${distance * 1000}&lat=${lat}&lng=${lng}&date=${date}&time=${time}&bezier=true&version=v2`, {
    "headers": {
      "accept": "*/*",
      "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
      "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"96\", \"Google Chrome\";v=\"96\"",
      "sec-ch-ua-mobile": "?0",
      "sec-ch-ua-platform": "\"Linux\"",
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "cross-site",
      "Referer": "https://www.smappen.com/",
      "Referrer-Policy": "strict-origin-when-cross-origin"
    },
    "body": null,
    "method": "GET"
  }).then(x => x.json()).then(x => console.log(x) || x).then(x => decodePolyLinetoCoordinates(x.polyline[0]).map((x) => x.reverse())).catch((err => {
    res.json({ error: err })
  }));



  const poly = `(${(polygonCoordinates || []).map((ele) => `(${ele.join(",")})`).join(",")})`

  const result = await db.query(`INSERT INTO site(siteid,name,location,polygon) VALUES ('${randomUUID()}','${name}','(${lat},${lng})','${poly}'::polygon)`)

  const indexedResponse = await fetch('http://localhost:3001/api/index-data');

  res.json({ polygonCoordinates })


})

module.exports = router;
