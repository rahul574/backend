const { Client } = require('pg')
const client = new Client({ host: 'localhost', port: 5432, password: 'password', user: 'postgres', database: 'postgres' })

client.connect()

module.exports = client